package mortalquest.character;

import mortalquest.model.Item;
import mortalquest.model.Location;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Roland Fuller
 */
public class CharacterTest {

    public static final String PLAYER_NAME = "Frank";
    public static final String ITEM_NAME = "Rusty Spoon";
    private Character playerCharacter;
    private Location testLocation, testLocation2;

    public CharacterTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        testLocation = new Location("Empty Room", "A plain white room with no windows, doors, or other features", Location.LocationConsequence.LOSE);
        testLocation2 = new Location("The Void", "Just.... Blackness", Location.LocationConsequence.LOSE);
        playerCharacter = new PlayerCharacter();
        playerCharacter.setName(PLAYER_NAME);
        playerCharacter.setLocation(testLocation);
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getHealth method, of class Character.
     */
    @Test
    public void testGetHealth() {
        System.out.println("getHealth");

        assertEquals(100, playerCharacter.getHealth());
        assertEquals(true, playerCharacter.isAlive());
        playerCharacter.hit(10);
        assertEquals(90, playerCharacter.getHealth());
        assertEquals(true, playerCharacter.isAlive());
        playerCharacter.hit(90);
        assertEquals(0, playerCharacter.getHealth());
        assertEquals(false, playerCharacter.isAlive());
    }

    /**
     * Test of getLocation method, of class Character.
     */
    @Test
    public void testGetSetLocation() {
        System.out.println("getLocation");
        assertEquals(testLocation, playerCharacter.getLocation());
        playerCharacter.setLocation(testLocation2);
        assertEquals(testLocation2, playerCharacter.getLocation());
    }

    /**
     * Test of getName method, of class Character.
     */
    @Test
    public void testGetName() {
        assertEquals(PLAYER_NAME, playerCharacter.getName());
    }

    /**
     * Test of giveItem method, of class Character.
     */
    @Test
    public void testGiveItem() {
        System.out.println("giveItem");
        Item item = new Item(ITEM_NAME);
        playerCharacter.giveItem(item);
        assertEquals(item, playerCharacter.getItems().contains(item));
        playerCharacter.takeItem(item);
        assertEquals(item, !playerCharacter.getItems().contains(item));
    }

    /**
     * Test of setName method, of class Character.
     */
    @Test
    public void testSetName() {
        System.out.println("setName");
        String name = "Bill";
        playerCharacter.setName(name);
        assertEquals(PLAYER_NAME, playerCharacter.getName());
    }

}
