package mortalquest.view.console;

import java.util.Scanner;
import java.util.logging.Logger;
import mortalquest.command.Command;
import mortalquest.command.Command.ResultCode;
import mortalquest.command.CommandHandler;
import mortalquest.controller.ControlCommand.LoadGameCommand;
import mortalquest.controller.ControlCommand.NewGameCommand;
import mortalquest.controller.ControlCommand.SaveGameCommand;
import mortalquest.model.GameCommand;
import mortalquest.view.View;
import mortalquest.view.ViewCommand;

/**
 *
 * @author Roland Fuller
 */
public class ConsoleView implements View {

    private static final Logger LOG = Logger.getLogger(ConsoleView.class.getName());
    public static final String MENU_MESSAGE = "Welcome to Mortal Quest. Please make your selection\n1) Start new game\n2) Load a saved game\n3) Save Game\n4) Quit";
    public static final String CHOOSE_ACTION_MESSAGE = "Please choose your action:\n1) move n[orth] | s[outh] | e[ast] | w[est]\n2) take name\n3) look\n4) quit (to menu)";
    public static final String INVALID_INPUT_MESSAGE = "Invalid input. Please try again";
    public static final String SAVING_GAME_MESSAGE = "Saving game";
    public static final String ENTER_NAME_MESSAGE = "Kay, what is your name?";

    private Scanner scanner = new Scanner(System.in);
    private CommandHandler commandHandler;

    @Override
    public void setCommandHandler(CommandHandler commandHandler) {
        this.commandHandler = commandHandler;
    }

    @Override
    public ResultCode handle(ViewCommand command) {
        return command.execute(this);
    }

    @Override
    public void drawText(String text) {
        System.out.println(text);
    }

    @Override
    public void drawImage(Picture image) {
        String piccy = "";
        switch (image) {
            case BG_1:
                piccy = "A background is drawn here using creative ASCII art.";
                break;
            case BG_2:
                piccy = "A different background is drawn here that's a bit lack-lustre";
                break;
            case PLAYER_CHARACTER:
                piccy = "An impressive 3d rendered model of the player character";
                break;
            case NPC_CHARACTER:
                piccy = "An unimpressive 3d rendered model of some character";
                break;
            case ITEM_BOTTLE:
                piccy = "A bottle. What more do you need to know?";
                break;
        }
        System.out.println(piccy);
    }

    public void menu() {
        boolean quit = false;
        do {
            System.out.println(MENU_MESSAGE);
            Command command = null;
            try {
                switch (Integer.parseInt(scanner.nextLine())) {
                    case 1:
                        System.out.println(ENTER_NAME_MESSAGE);
                        command = new NewGameCommand(scanner.nextLine());
                        play();
                        break;
                    case 2:
                        System.out.println(ENTER_NAME_MESSAGE);
                        command = new LoadGameCommand(scanner.nextLine());
                        play();
                        break;
                    case 3:
                        System.out.println(SAVING_GAME_MESSAGE);
                        command = new SaveGameCommand();
                    case 4:
                        quit = true;
                }
            } catch (NumberFormatException exception) {
                System.out.println(INVALID_INPUT_MESSAGE);
            }
            sendCommand(command);
        } while (!quit);
        System.out.println("Bye!");
    }

    private void play() {
        boolean playing = true;
        do {
            System.out.println(CHOOSE_ACTION_MESSAGE);
            playing = parse(scanner.nextLine().split(" "));
        } while (playing);
    }

    private boolean parse(String[] inputs) {
        Command command = null;
        if (inputs.length > 0) {
            switch (inputs[0]) {
                case "move":
                    command = GameCommand.getMoveCommand(inputs);
                    break;
                case "take":
                    command = GameCommand.getTakeCommand(inputs);
                    break;
                case "look":
                    command = GameCommand.getLookCommand();
                    break;
                case "quit":
                    return false;
                default:
                    System.out.println(INVALID_INPUT_MESSAGE);
                    break;
            }
        }
        sendCommand(command);
        return true;
    }

    private void sendCommand(Command command) {
        if (command != null) {
            if (commandHandler.handle(command).equals(ResultCode.FAILURE)) {
                System.out.println(INVALID_INPUT_MESSAGE);
            }
        }
    }
}
