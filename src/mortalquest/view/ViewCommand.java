package mortalquest.view;

import java.util.Collections;
import java.util.List;
import mortalquest.character.Character;
import mortalquest.command.Command;
import mortalquest.model.Item;
import mortalquest.model.Location;
import mortalquest.model.Model;

/**
 * This command shows a better example of how the command logic can control what the interface allows it to do. In this
 * case, the command only knows about the view interface, but can perform complex operations on it without knowing how
 * it works. This has the dual benefit of hiding the details away in the command class, making the view
 * implementation(s) simpler and reducing duplication.
 *
 * Open/closed principle "software entities … should be open for extension, but closed for modification."
 *
 * @author Roland Fuller
 */
public class ViewCommand implements Command<View> {

    private final Model model;

    public ViewCommand(Model model) {
        this.model = model;
    }

    @Override
    public ResultCode execute(View view) {

        Character player = model.getPlayer();
        Location location = model.getPlayer().getLocation();

        view.drawText(player.getName() + " Has got " + player.getHealth() + " health");
        view.drawImage(View.Picture.PLAYER_CHARACTER); // Drawing an "image" on a console only makes sense if that image is an ascii character or ascii art string
        // the console view could have a null implementation of this method whereas the gui can implement it fully, for example

        view.drawText("and is currently at the " + location.getName());
        view.drawText(location.getDescription());

        if (location.getItems().size() > 0) {
            view.drawText("Here, you see the following items: ");
            List<Item> items = location.getItems();
            Collections.sort(items); // sort items alphabetically
            for (Item item : items) {
                view.drawText(item.getName());
                view.drawImage(View.Picture.ITEM_BOTTLE); // all items look like bottles for now - need to store image in item class
            }
        }

        if (location.getCharacters().size() > 0) {
            view.drawText("And these guys are also here: ");
            for (Character npc : location.getCharacters()) {
                view.drawText(player.getName());
                view.drawImage(View.Picture.NPC_CHARACTER); // all items look like bottles for now - need to store image in item class
            }
        }

        return ResultCode.SUCCESS;
    }

}
