package mortalquest.view;

import mortalquest.Component;

/**
 * An interface which allows a command or other class to tell the view what to do without knowing the implementation
 * details of whichever specific view-type object we may be talking to
 *
 * Single responsibility principle a class should have only a single responsibility (i.e. changes to only one part of
 * the software's specification should be able to affect the specification of the class).
 *
 * @author Roland Fuller
 */
public interface View extends Component<ViewCommand> {

    public static final String GAME_TITLE = "MORTAL QUEST";

    public enum Picture {
        PLAYER_CHARACTER, NPC_CHARACTER, ITEM_BOTTLE, BG_1, BG_2
    };

    void drawText(String text);

    void drawImage(Picture image);

}
