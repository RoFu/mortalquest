package mortalquest.view.gui;

import java.io.IOException;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import mortalquest.Component;
import static mortalquest.view.View.GAME_TITLE;

/**
 *
 * @author Roland Fuller
 */
public class GUIApplication extends Application {

    private static final Logger LOG = Logger.getLogger(GUIApplication.class.getName());
    public static final String FXML_FILENAME = "GUIView.fxml";
    private static Component controller;

    public static void setController(Component theController) {
        controller = theController;
    }

    @Override
    public void start(Stage stage) throws IOException {
        stage.setTitle(GAME_TITLE);

        FXMLLoader fxmlLoader = new FXMLLoader();
        VBox root = fxmlLoader.load(getClass().getResourceAsStream(FXML_FILENAME));
        GUIView fooController = (GUIView) fxmlLoader.getController();
        controller.setCommandHandler(fooController);
        fooController.setCommandHandler(controller);

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

}
