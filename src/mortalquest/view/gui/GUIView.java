package mortalquest.view.gui;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import mortalquest.command.Command;
import mortalquest.command.CommandHandler;
import mortalquest.controller.ControlCommand;
import mortalquest.model.GameCommand;
import mortalquest.view.View;
import mortalquest.view.ViewCommand;

/**
 *
 * @author Roland Fuller
 */
public class GUIView implements View {

    // @FXML annotations link these variables to the actual GUI objects with the same names
    @FXML
    private Text status;
    @FXML
    private Button moveNorth;
    @FXML
    private Button moveSouth;
    @FXML
    private Button moveEast;
    @FXML
    private Button moveWest;
    @FXML
    private ImageView canvas;

    // will refer to the controller
    private CommandHandler commandHandler;

    // need to load up more images if you want to display interesting things
    private Image image1;

    public GUIView() {
        try {
            image1 = new Image("file:src/images/test.png");
        } catch (Exception ex) {
            status.setText("ERROR: Image file failed to load!!!");
        }
    }

    @Override
    public void setCommandHandler(CommandHandler<?> commandHandler) {
        this.commandHandler = commandHandler;
    }

    @Override
    public Command.ResultCode handle(ViewCommand command) {
        return command.execute(this);
    }

    @Override
    public void drawText(String text) {
        // This is pretty horrible since there is no wrapping and the text is never cleared.....
        // it would be ideal to scroll the text, deleting the oldest lines and keeping a fixed length output...
        status.setText(status.getText().concat(text));
    }

    // This method of the view interface may be fleshed out with coordinates,
    // in this game probably fixed coords would suffice...
    @Override
    public void drawImage(Picture image) {
        switch (image) {
            case BG_1:
                canvas.setImage(image1);
                break;
            case BG_2:
                canvas.setImage(image1);
                break;
            case ITEM_BOTTLE:
                canvas.setImage(image1);
                break;
            case NPC_CHARACTER:
                canvas.setImage(image1);
                break;
            case PLAYER_CHARACTER:
                canvas.setImage(image1);
                break;
        }
    }

    // The following methods are event handlers for the buttons on the GUI
    // the effects are the same as in the console view.... creating the same kinds of commands
    // for processing by the controller.
    @FXML
    protected void begin(ActionEvent event) {
        status.setText("begin new game");
        commandHandler.handle(new ControlCommand.NewGameCommand("Get player name from the GUI"));
    }

    @FXML
    protected void load(ActionEvent event) {
        status.setText("load");
        commandHandler.handle(new ControlCommand.LoadGameCommand("Get player name from the GUI"));
    }

    @FXML
    protected void save(ActionEvent event) {
        status.setText("save");
        commandHandler.handle(new ControlCommand.SaveGameCommand());
    }

    @FXML
    protected void move(ActionEvent event) {
        if (event.getSource().equals(moveNorth)) {
            commandHandler.handle(GameCommand.getMoveCommand(new String[]{"", "n"}));
        } else if (event.getSource().equals(moveSouth)) {
            commandHandler.handle(GameCommand.getMoveCommand(new String[]{"", "s"}));
        } else if (event.getSource().equals(moveEast)) {
            commandHandler.handle(GameCommand.getMoveCommand(new String[]{"", "e"}));
        } else if (event.getSource().equals(moveWest)) {
            commandHandler.handle(GameCommand.getMoveCommand(new String[]{"", "w"}));
        }
    }

}
