package mortalquest.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import mortalquest.character.Character;

/**
 ** If we thought about the design a little further, this class may be able to become package protected
 *
 * @author Roland Fuller
 */
public class Location implements Serializable {

    public enum LocationConsequence {
        NOTHING, WIN, LOSE
    }

    public enum Direction {
        NORTH, SOUTH, EAST, WEST
    }
    private String name;
    private String description;
    private LocationConsequence consequence;
    private java.util.Map<Direction, Location> compass;
    private List<Character> players;
    private List<Item> items;

    /**
     * Serialisation constructor
     */
    public Location() {
    }

    public Location(String name, String description) {
        this(name, description, LocationConsequence.NOTHING);
    }

    public Location(String name, String description, LocationConsequence consequence) {
        this.consequence = consequence;
        this.name = name;
        this.description = description;
        compass = new HashMap<>();
        players = new ArrayList<>();
        items = new ArrayList<Item>();
    }

    public void setLink(Direction direction, Location location) {
        compass.put(direction, location);
    }

    public void enter(Character player) {
        player.setLocation(this);
        this.players.add(player);
    }

    public void exit(Character player) {
        this.players.remove(player);
    }

    public Location compass(Direction direction) {
        return compass.get(direction);
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public List<Item> getItems() {
        return items;
    }

    public Item takeItem(String name) {
        Item taken = null;
        for (Item item : items) {
            if (item.getName().toLowerCase().equals(name.toLowerCase())) {
                taken = item;
            }
        }
        items.remove(taken);
        return taken;
    }

    public List<Character> getCharacters() {
        return players;
    }

    void addItem(Item item) {
        items.add(item);
    }
}
