package mortalquest.model;

import java.util.Collections;
import java.util.List;
import mortalquest.character.Character;
import mortalquest.command.Command;
import mortalquest.controller.ControlCommand;
import mortalquest.model.Location.Direction;

/**
 * Game Commands are messages that the user can give to the game to perform an action within the game play itself
 *
 * Open/closed principle "software entities … should be open for extension, but closed for modification."
 *
 * @author Roland Fuller
 */
public abstract class GameCommand implements Command<Model> {

    protected final String[] input;

    GameCommand(String[] input) {
        this.input = input;
    }

    /**
     * Factory methods to perform the complex action of wrapping the requested game command object into a control
     * command so that it can be handled by the controller by seamlessly passing it on to the model.
     *
     * @param inputs
     * @return
     */
    public static Command<?> getMoveCommand(String[] inputs) {
        return new ControlCommand.ModelCommand(new MoveCommand(inputs));
    }

    public static Command<?> getTakeCommand(String[] inputs) {
        return new ControlCommand.ModelCommand(new TakeCommand(inputs));
    }

    public static Command<?> getLookCommand() {
        return new ControlCommand.ModelCommand(new LookCommand());
    }

    protected abstract boolean isValid(Object... args);

    /**
     * The following command classes are private because the above factory methods should be the only way to access
     * instances.
     */
    private static class MoveCommand extends GameCommand {

        private Direction direction;

        protected MoveCommand(String[] input) {
            super(input);
        }

        @Override
        public ResultCode execute(Model commandHandler) {
            if (isValid(input)) {
                commandHandler.move(direction);
                return ResultCode.SUCCESS;
            }
            return ResultCode.FAILURE;
        }

        @Override
        public boolean isValid(Object... args) {
            String dir = args[1].toString();
            if (dir.length() > 0) {
                switch (dir.toLowerCase().charAt(0)) {
                    case 'n':
                        direction = Direction.NORTH;
                        return true;
                    case 's':
                        direction = Direction.SOUTH;
                        return true;
                    case 'e':
                        direction = Direction.EAST;
                        return true;
                    case 'w':
                        direction = Direction.WEST;
                        return true;
                }
            }
            return false;
        }
    }

    // This class is untested but gives an example.
    // The main problem is with the input processing
    private static class TakeCommand extends GameCommand {

        protected TakeCommand(String[] input) {
            super(input);
        }

        @Override
        public ResultCode execute(Model commandHandler) {
            Character player = commandHandler.getPlayer();
            if (isValid(input, player.getLocation().getItems())) {
                commandHandler.take(player, input[1]);
                return ResultCode.SUCCESS;
            }
            return ResultCode.FAILURE;
        }

        @Override
        public boolean isValid(Object... args) {
            String itemName = args[0].toString();
            // use searching from collections here
            List<Item> items = (List) args[1];
            Collections.sort(items);
            if (Collections.binarySearch(items, new Item(itemName)) > -1) {
                return true;
            } else {
                return false;
            }
        }
    }

    private static class LookCommand extends GameCommand {

        protected LookCommand() {
            super(null);
        }

        @Override
        protected boolean isValid(Object... args) {
            return true;
        }

        @Override
        public ResultCode execute(Model commandHanlingInterface) {
            commandHanlingInterface.update();
            return ResultCode.SUCCESS;
        }

    }
}
