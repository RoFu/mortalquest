package mortalquest.model;

import java.io.Serializable;
import java.util.logging.Logger;
import mortalquest.Component;
import mortalquest.character.AICharacter;
import mortalquest.character.Character;
import mortalquest.character.behaviour.Attack;
import mortalquest.character.behaviour.Converse;
import mortalquest.command.Command.ResultCode;
import mortalquest.command.CommandHandler;
import mortalquest.model.Location.Direction;
import mortalquest.view.ViewCommand;

/**
 * All classes under model need to be serialisable as we will save the whole object to disk this includes all the
 * objects it contains like map, characters, items, etc.
 *
 * Single responsibility principle a class should have only a single responsibility (i.e. changes to only one part of
 * the software's specification should be able to affect the specification of the class).
 *
 * @author Roland Fuller
 */
public class Model implements Component<GameCommand>, Serializable {

    private static final Logger LOG = Logger.getLogger(Model.class.getName());
    private Character player;
    private Map map;
    private transient CommandHandler commandHandler; // Don't serialise this. Set it up to the new view object.
    // i.e we can't be expected to serialise the whole view as well in a save game.

    @Override
    public void setCommandHandler(CommandHandler commandHandler) {
        this.commandHandler = commandHandler;
    }

    @Override
    public ResultCode handle(GameCommand command) {
        return command.execute(this);
    }

    /**
     * Serialisation constructor
     */
    public Model() {
        map = new Map();

        // The following character stuff doesn't do anything because the game
        // doesn't support them within the map fully
        AICharacter shopKeeper = new AICharacter(new Converse());
        shopKeeper.update();

        AICharacter enemy = new AICharacter(new Attack());
        enemy.update();
    }

    /**
     * This could be extended to move any character
     *
     * @param direction
     */
    public void move(Direction direction) {
        Location currentLocation = player.getLocation();
        Location destination = currentLocation.compass(direction);
        if (destination != null) {
            currentLocation.exit(player);
            destination.enter(player);
            update();
        }
    }

    public void take(Character character, String itemName) {
        Item item = null;
        item = character.getLocation().takeItem(itemName);
        if (!character.giveItem(item)) {
            character.getLocation().addItem(item);
        }
    }

    public void setPlayer(Character character) {
        this.player = character;
        character.setLocation(map.getStartLocation());
        update();
    }

    public Character getPlayer() {
        return player;
    }

    void update() {
        commandHandler.handle(new ViewCommand(this));
    }

}
