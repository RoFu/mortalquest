package mortalquest.model;

import java.io.Serializable;

/**
 * If we thought about the design a little further, this class may be able to become package protected
 *
 * @author Roland Fuller
 */
public class Item implements Serializable, Comparable<Item> {

    private String name;

    /**
     * Serialisation constructor
     */
    public Item() {
    }

    public Item(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public int compareTo(Item o) {
        return name.compareTo(o.getName());
    }

}
