package mortalquest.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import mortalquest.model.Location.Direction;

/**
 * Package protected as this is solely part of the model code.
 *
 * @author Roland Fuller
 */
class Map implements Serializable {

    private List<Location> locations;
    private Location start;

    /**
     * Serialisation constructor
     */
    public Map() {
        locations = new ArrayList<>();
        defaultMap();
    }

    /**
     * This method is here purely for testing purposes. It constitutes a hard-coded map data. This should only be
     * available via the database tables, or serialised data file. We should never have to manually do this stuff,
     * unless we were to write a procedural generation algorithm for random maps.
     */
    private void defaultMap() {
        start = new Location("Start", "A grassy field lays before you with lush long blades gently swaying in the soft summer breeze...");
        start.addItem(new Item("Handful of grass"));
        Location mountain = new Location("Mountain top", "A chilly place to be, covered in ice and snow...");
        mountain.addItem(new Item("Snowball"));
        Location canyon = new Location("canyon", "A yawning chasm gouged deep into sandy stone by eons of elemental persuasion...");
        mountain.addItem(new Item("Rock"));
        Location ravine = new Location("ravine", "A Ravine. What is a ravine, anyway?");
        mountain.addItem(new Item("Awesome Sauce"));
        Location slope = new Location("slope", "A dangerously sharp angled place...");
        mountain.addItem(new Item("bundle of sticks"));
        Location kiosk = new Location("kiosk", "Warm kiosk with cheesy merchandise. Would you like fries with that");
        mountain.addItem(new Item("Souvenir keychain"));

        start.setLink(Direction.EAST, mountain);
        mountain.setLink(Direction.WEST, start);
        mountain.setLink(Direction.NORTH, canyon);
        mountain.setLink(Direction.EAST, slope);
        mountain.setLink(Direction.SOUTH, ravine);
        slope.setLink(Direction.SOUTH, kiosk);
        kiosk.setLink(Direction.NORTH, slope);
        kiosk.setLink(Direction.WEST, start);

        locations.add(start);
        locations.add(mountain);
        locations.add(canyon);
        locations.add(ravine);
        locations.add(slope);
        locations.add(kiosk);
    }

    public void addLocation(Location location) {
        locations.add(location);
    }

    public void addLocation(Location location, boolean isStart) {
        start = location;
        addLocation(location);
    }

    public void linkLocations(Location from, Location to, Direction direction) {
        from.setLink(direction, to);
    }

    public Location getStartLocation() {
        return start;
    }
}
