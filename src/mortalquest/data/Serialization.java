package mortalquest.data;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Basic serialization utility functions to talk to a disk file in this case.
 *
 * Open/closed principle "software entities … should be open for extension, but closed for modification."
 *
 * @author Roland Fuller
 */
class Serialization {

    private static final Logger LOG = Logger.getLogger(Serialization.class.getName());
    public static final String FILE_EXTENSION = ".save";

    // Save a given object data to a specified filename
    public void save(Object object, String fileName) {
        FileOutputStream fileOutputStream;
        try {
            fileOutputStream = new FileOutputStream(fileName + FILE_EXTENSION);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(object);
            objectOutputStream.flush();
            objectOutputStream.close();
        } catch (FileNotFoundException ex) {
            LOG.log(Level.SEVERE, "File not found when trying to serialize to file");
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, ex.getMessage());
        }
    }

    // load in a requested object (Type) from a specified filename
    // this is an example of a generic method, which casts the loaded in object
    // to the requested type. Note that it also throws a classcast exception,
    // if the loaded data does not match the requested object type
    // Using the generic here makes this class more reusable as we don't need to
    // refer to application specific classes
    public <T> T load(String fileName) throws ClassCastException {
        Object object = null;
        try {
            FileInputStream fileInputStream = new FileInputStream(fileName + FILE_EXTENSION);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            object = objectInputStream.readObject();
            objectInputStream.close();
        } catch (FileNotFoundException ex) {
            LOG.log(Level.SEVERE, "File not found when trying to serialize to file");
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, ex.getMessage());
        } catch (ClassNotFoundException ex) {
            LOG.log(Level.SEVERE, "Failed to convert serialised data to requested class");
        }
        return (T) object;
    }

}
