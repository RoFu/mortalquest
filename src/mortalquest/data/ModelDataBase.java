package mortalquest.data;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import mortalquest.model.Model;

/**
 * An implementation of the JDBCDataBase class which extends that basic functionality and also implements the DataSource
 * interface so that our game has now got a concrete object which can talk to databases AND fill the role of a model
 * saver/loader without exposing its implementation details
 *
 * @author Roland Fuller
 */
public class ModelDataBase extends JDBCDataBase implements DataSource {

    private static final String MODEL_TABLE = "model";
    private static final String ID_COLUMN = "id";

    private static final String LOAD_QUERY = "select * from " + MODEL_TABLE + " where " + ID_COLUMN + " = ?";
    private static final String SAVE_QUERY = "insert into " + MODEL_TABLE + " values(?,?,?,?,?,?,?,?)";

    @Override
    public Model loadModel(String name) {
        Model model = null;
        LOG.log(Level.FINE, "Loading model from database");
        if (connect()) {
            PreparedStatement getModel = null;
            try {
                connection.setAutoCommit(false);
                getModel = connection.prepareStatement(LOAD_QUERY);
                getModel.setString(1, name);
                ResultSet rs = getModel.executeQuery();
                if (rs.next()) {
                    model = new Model();
                    // read out model properties including locations, items, characters...
                    // recompose the data into the model object
                } else {
                    throw new IllegalArgumentException();
                }
                connection.commit();
                connection.setAutoCommit(true);
            } catch (SQLException ex) {
                LOG.log(Level.SEVERE, null, ex);
            } catch (IllegalArgumentException ex) {
                LOG.log(Level.SEVERE, null, "Could not load model - probably doesn't exist");
            }
        }
        return model;
    }

    @Override
    public void saveModel(Model model) {
        LOG.log(Level.FINE, "Saving model to database");
        if (connect()) {
            PreparedStatement saveModel = null;
            try {
                connection.setAutoCommit(false);
                saveModel = connection.prepareStatement(SAVE_QUERY);
                // Set up parameters here i.e all the various bits of data that need inserting
                ResultSet rs = saveModel.executeQuery();
                connection.commit();
                connection.setAutoCommit(true);
            } catch (SQLException ex) {
                LOG.log(Level.SEVERE, null, ex);
            }
        }
    }
}
