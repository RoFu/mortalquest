package mortalquest.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * An basic implementation of JDBC database connectivity we should extend into a specific concrete use
 *
 * Open/closed principle "software entities … should be open for extension, but closed for modification."
 *
 * @author Roland Fuller
 */
abstract class JDBCDataBase {

    protected static final Logger LOG = Logger.getLogger(JDBCDataBase.class.getName());
    protected Connection connection;
    private static final String DEFAULT_DB_TYPE = "mysql";
    private static final String DEFAULT_DB_HOST = "localhost";
    private static final String DEFAULT_DB_PORT = "3306";
    private static final String DEFAULT_DB_NAME = "mortalquest";
    private static final String DEFAULT_DB_USERNAME = "root";
    private static final String DEFAULT_DB_PASSWORD = "";
    private final String type;
    private final String host;
    private final String port;
    private final String database;
    private final String userName;
    private final String password;

    /**
     * If no parameters are supplpied, the defaults are used.
     */
    public JDBCDataBase() {
        this(DEFAULT_DB_TYPE, DEFAULT_DB_HOST, DEFAULT_DB_PORT, DEFAULT_DB_NAME, DEFAULT_DB_USERNAME, DEFAULT_DB_PASSWORD);
    }

    /**
     * Package protected so that only child classes can be seen or instantiated
     *
     * @param type the kind of DB to connect to
     * @param host The location of the database server
     * @param port the port number the dbms service is running on
     * @param database the name of the database to talk to
     * @param userName database user logging in
     * @param password database user's password
     */
    JDBCDataBase(String type, String host, String port, String database, String userName, String password) {
        this.type = type;
        this.host = host;
        this.port = port;
        this.database = database;
        this.userName = userName;
        this.password = password;
    }

    /**
     * Protected so that only child classes can control this. Ideally we need to implement some connection pooling stuff
     * if the application is large enough to warrant it.
     *
     * @return whether or not the connection succeeded.
     */
    protected boolean connect() {
        if (connection == null) {
            LOG.log(Level.FINE, "Connecting to database...");
            try {
                connection = DriverManager.getConnection("jdbc:" + type + "://" + host + ":" + port + "/" + database, userName, password);
                return true;
            } catch (SQLException ex) {
                LOG.log(Level.SEVERE, null, ex);
            }
        }
        return false;
    }
}
