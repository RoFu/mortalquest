package mortalquest.data;

import mortalquest.model.Model;

/**
 * A simple interface for saving and loading models which separates the implementation from its usage.
 *
 * @author Roland Fuller
 */
public interface DataSource {

    Model loadModel(String name);

    void saveModel(Model model);
}
