package mortalquest.data;

import java.util.logging.Level;
import java.util.logging.Logger;
import mortalquest.model.Model;

/**
 * Like the ModelDataBase class, this is backed by basic serialisation utilities and fills the role of a DataSource so
 * the application can use this type of object to save and load models agnostic of the way in which that is achieved.
 *
 * @author Roland Fuller
 */
public class ModelSerialization extends Serialization implements DataSource {

    private static final Logger LOG = Logger.getLogger(ModelSerialization.class.getName());

    @Override
    public Model loadModel(String name) {
        LOG.log(Level.FINE, "loading model from serialized file");
        return load(name);
    }

    @Override
    public void saveModel(Model model) {
        LOG.log(Level.FINE, "Saving model to serialized file");
        save(model, model.getPlayer().getName());
    }

}
