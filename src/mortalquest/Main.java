package mortalquest;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import mortalquest.controller.Controller;
import mortalquest.data.ModelSerialization;
import mortalquest.view.View;
import mortalquest.view.console.ConsoleView;
import mortalquest.view.gui.GUIApplication;

/**
 * The entry point of the program. Examines command line parameters to determine whether we should launch the console or
 * gui version of the program.
 *
 * Single responsibility principle a class should have only a single responsibility (i.e. changes to only one part of
 * the software's specification should be able to affect the specification of the class).
 *
 * Dependency inversion principle one should "depend upon abstractions, [not] concretions." - applied in the case of
 * data source and M/V/C objects
 *
 * @author Roland Fuller
 */
public class Main {

    private static final Logger LOG = Logger.getLogger(Main.class.getName());
    private static final String CONSOLE = "console";
    private static Controller controller;
    private static View view;

    public static void main(String[] args) {
        boolean console = false;
        controller = new Controller(new ModelSerialization());

        // check if command line arguments contains the word console
        for (String arg : args) {
            if (arg.contains(CONSOLE)) {
                console = true;
            }
        }

        if (console) {
            LOG.log(Level.ALL, "Launching Console version");
            view = new ConsoleView();
            controller.setCommandHandler(view);
            view.setCommandHandler(controller);
            ((ConsoleView) view).menu();
        } else {
            LOG.log(Level.ALL, "Launching GUI version");
            GUIApplication.setController(controller);
            Application.launch(GUIApplication.class);
        }
    }
}
