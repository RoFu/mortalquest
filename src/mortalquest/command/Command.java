package mortalquest.command;

/**
 * An implementation of the Command pattern
 *
 * @author Roland Fuller
 * @param <T> is the interface upon which a command should execute
 */
public interface Command<T> {

    /**
     * A simple mechanism for signalling to the caller of a command's execution whether or not it was successful. This
     * functionality would ideally be encapsulated into the command classes, and the result code being promoted to a
     * class itself, to give very complex options for what and how results are returned.
     */
    public enum ResultCode {
        SUCCESS, FAILURE

    }

    // The generic type T represents the class upon which the command should act
    public ResultCode execute(T commandHandler);
}
