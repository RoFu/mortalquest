package mortalquest.command;

import mortalquest.command.Command.ResultCode;

/**
 * Observer/Subscriber pattern combined with command pattern allows an object to recieve (only) a specified (familiy of)
 * command objects to execute
 *
 * @author Roland Fuller
 * @param <T>
 */
public interface CommandHandler<T extends Command<?>> {

    public ResultCode handle(T command);
}
