package mortalquest.controller;

import mortalquest.character.PlayerCharacter;
import mortalquest.command.Command;
import mortalquest.model.GameCommand;

/**
 * Command pattern types which are destined to be interpretted/executed by the controller section of the program. Please
 * note the static nested classes are used to avoid a massive proliferation of classes since each sub class is fairly
 * trivial we can afford to group them together in one file, and this gives another level of organisation, having all
 * sub types appear within the super type.
 *
 * This idea is used again for the other high level commands.
 *
 * We could employ a factory method pattern here to give a consistent and encapsulated way to create/obtain different
 * command objects. But for now there isn't too much benefit.
 *
 * Open/closed principle "software entities … should be open for extension, but closed for modification."
 *
 * @author Roland Fuller
 */
public abstract class ControlCommand implements Command<Controller> {

    /**
     * We want to load a previously saved game. The name of the save is provided.
     */
    public static class LoadGameCommand extends ControlCommand {

        private final String fileName;

        public LoadGameCommand(String fileName) {
            this.fileName = fileName;
        }

        public ResultCode execute(Controller commandHandler) {
            return commandHandler.loadGame(fileName) ? ResultCode.SUCCESS : ResultCode.FAILURE;
        }
    }

    /**
     * We want to start a fresh game. The name of the player is provided.
     */
    public static class NewGameCommand extends ControlCommand {

        private final String name;

        public NewGameCommand(String name) {
            this.name = name;
        }

        @Override
        public ResultCode execute(Controller commandHandler) {
            PlayerCharacter player = new PlayerCharacter();
            player.setName(name);
            commandHandler.newGame(player);
            return ResultCode.SUCCESS;
        }

    }

    /**
     * We want to save a game in progress.
     */
    public static class SaveGameCommand extends ControlCommand {

        @Override
        public ResultCode execute(Controller commandHandler) {
            return commandHandler.saveGame() ? ResultCode.SUCCESS : ResultCode.FAILURE;
        }
    }

    /**
     * The model command is used to wrap game commands which are being sent from the view to the controller. It is a
     * clean way to 'trick' the controller into providing direct access to the model so that the game command can be
     * executed there. So the controller is unaware it has passed on the command and we don't need some logical check to
     * divert the flow of commands to the model.
     *
     * @see GameCommand
     */
    public static class ModelCommand extends ControlCommand {

        private final GameCommand gameCommand;

        public ModelCommand(GameCommand gameCommand) {
            this.gameCommand = gameCommand;
        }

        @Override
        public ResultCode execute(Controller commandHandler) {
            return commandHandler.getModel().handle(gameCommand);
        }
    }
}
