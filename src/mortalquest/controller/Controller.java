package mortalquest.controller;

import java.util.logging.Logger;
import mortalquest.Component;
import mortalquest.character.PlayerCharacter;
import mortalquest.command.Command.ResultCode;
import mortalquest.command.CommandHandler;
import mortalquest.data.DataSource;
import mortalquest.model.Model;

/**
 * The primary controller for the game. At the moment it can start a new game, load a game from disk or save a game in
 * progress to disk and allow game commands to flow through to the model.
 *
 * It also demonstrates a use of dependency injection (The DataSource object given to the controller - we are agnostic
 * here about whether that is backed by a database or serialisation)
 *
 * Single responsibility principle a class should have only a single responsibility (i.e. changes to only one part of
 * the software's specification should be able to affect the specification of the class).
 *
 * @author Roland Fuller
 */
public class Controller implements Component<ControlCommand> {

    private static final Logger LOG = Logger.getLogger(Controller.class.getName());
    public static final String DEFAULT_MODEL = "DEFAULT";
    private Model model;
    private DataSource dataSource;

    // represents the view in this case, so we can inform the model where it is without further complicating our design with 
    // something like a service locator, and keep consistency between our component classes. 
    // Note there isn't really anything to enforce this
    // Any valid command handler could be set without our knowledge which may be good or bad.
    private CommandHandler commandHandler;

    public Controller(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void setCommandHandler(CommandHandler commandHandler) {
        this.commandHandler = commandHandler;
    }

    @Override
    public ResultCode handle(ControlCommand command) {
        return command.execute(this);
    }

    /**
     * Note this is package protected, so that only ControlCommand classes can access this. (Better encapsulation -
     * don't leak the Model)
     *
     * @return the current model in play
     */
    Model getModel() {
        return model;
    }

    /**
     * An entire new model is constructed. If the view supported a character creation screen, the player can be passed
     * into the new world.
     *
     * Package protected so only the control commands can access this functionality.
     *
     * @param player
     */
    void newGame(PlayerCharacter player) {
        //model = dataSource.loadModel(DEFAULT_MODEL);
        model = new Model();
        getModel().setCommandHandler(commandHandler);
        getModel().setPlayer(player);
    }

    /**
     * Load up a model from the datasource, where it gets the data from we don't know. Package protected so only the
     * control commands can access this functionality.
     *
     * @param saveName the name of the save game
     * @return
     */
    boolean loadGame(String saveName) {
        try {
            model = dataSource.loadModel(saveName);
            model.setCommandHandler(commandHandler);
            return true;
        } catch (ClassCastException cce) {
            return false;
        }
    }

    /**
     * Save the current model to some persistent storage (where ever the datasource happens to be pointing) Package
     * protected so only the control commands can access this functionality.
     *
     * @return
     */
    boolean saveGame() {
        try {
            dataSource.saveModel(model);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

}
