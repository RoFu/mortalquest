package mortalquest.character;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import mortalquest.model.Item;
import mortalquest.model.Location;

/**
 * The command base class for all kinds of characters in the game whether controlled by a human or AI
 *
 * @author Roland Fuller
 */
public abstract class Character implements Serializable {

    private static final Logger LOG = Logger.getLogger(Character.class.getName());
    protected int health;
    protected List<Item> items;
    protected Location location;
    protected String name;
    private boolean alive;

    public Character() {
        items = new ArrayList<>();
        alive = true;
    }

    public int getHealth() {
        return health;
    }

    public Location getLocation() {
        return this.location;
    }

    public String getName() {
        return name;
    }

    public boolean giveItem(Item item) {
        if (items.size() < 5) {
            items.add(item);
            return true;
        }
        return false;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public void setName(String name) {
        if (this.name == null) {
            this.name = name;
        } else {
            LOG.log(Level.ALL, "Player already has name ");
        }
    }

    void hit(int damage) {
        health -= damage;
        if (health <= 0) {
            alive = false;
        }
    }

    public boolean isAlive() {
        return alive;
    }

    public List<Item> getItems() {
        return items;
    }

    public void takeItem(Item item) {
        items.remove(item);
    }

}
