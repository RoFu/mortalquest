package mortalquest.character.behaviour;

/**
 * Another strategy which AI characters might employe Again, it requires a lot of fleshing out before it can be actually
 * useful.
 *
 * @author Roland Fuller
 */
public class Converse implements Behaviour {

    @Override
    public Behaviour behave() {
        System.out.println("Oh hai thar");
        return this;
    }

}
