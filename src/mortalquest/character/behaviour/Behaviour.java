package mortalquest.character.behaviour;

/**
 * Strategy pattern interface which determines what all behaviour strategies can do. At the moment it just gives an
 * implementation the opportunity to execute its strategy and optionally return a new strategy (state transition)
 *
 * Ideally, the strategy pattern would fit well with a sandbox pattern wherein each strategy inherits from a base class
 * that has an interface which will allow the strategies to access elements from the character on which it is acting as
 * well as potentially communicate with other strategies to co-ordinate sophisticated behaviour.
 *
 * Single responsibility principle a class should have only a single responsibility (i.e. changes to only one part of
 * the software's specification should be able to affect the specification of the class).
 *
 * Liskov substitution principle "objects in a program should be replaceable with instances of their subtypes without
 * altering the correctness of that program." See also design by contract.
 *
 * @author Roland Fuller
 */
public interface Behaviour {

    public Behaviour behave();
}
