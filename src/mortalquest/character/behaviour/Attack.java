package mortalquest.character.behaviour;

/**
 * Attacking behaviour strategy. This is very skeletal and would require access to the other character object which it
 * is attacking to send the required message
 *
 * @author Roland Fuller
 */
public class Attack implements Behaviour {

    @Override
    public Behaviour behave() {
        System.out.println("RARGH!!!");
        return new Converse();
    }

}
