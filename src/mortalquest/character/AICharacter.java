package mortalquest.character;

import java.io.Serializable;
import mortalquest.character.behaviour.Behaviour;

/**
 * Represents a NPC in the game. Related to the Player via the Character base class The NPC has a behaviour strategy
 * pattern implementation to determin its behaviour
 *
 * @author Roland Fuller
 */
public class AICharacter extends Character implements Serializable {

    private Behaviour behaviour;

    public AICharacter(Behaviour behaviour) {
        this.behaviour = behaviour;
    }

    /**
     * Allow the character a chance to think - executing its current behaviour. The behaviour strategy currently in
     * place may choose to transition to another state.
     */
    public void update() {
        behaviour = behaviour.behave();
    }
}
