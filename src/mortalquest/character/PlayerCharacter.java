package mortalquest.character;

import java.io.Serializable;

/**
 * A human controlled character. At the moment there isn't too much to specify for this kind but may be in the future
 * once more features are implemented.
 *
 * @author Roland Fuller
 */
public class PlayerCharacter extends Character implements Serializable {

    public PlayerCharacter() {
        health = 100;
    }

}
